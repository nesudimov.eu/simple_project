#!./venv/bin/python3

from flask import Flask

app = Flask(__name__)

@app.route('/')
def there():
  return '<a href="http://127.0.0.1:5000/here">Туда</a>'

@app.route('/here')
def here():
  return '<a href="http://127.0.0.1:5000/back">Сюда</a>'

@app.route('/back')
def back():
  return '<a href="http://127.0.0.1:5000/">Обратно</a>'

if __name__ == "__main__":
  app.run()
